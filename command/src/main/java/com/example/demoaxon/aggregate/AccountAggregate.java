package com.example.demoaxon.aggregate;


import com.example.demoaxon.command.CreateAccountCommand;
import com.example.demoaxon.command.CreditMoneyCommand;
import com.example.demoaxon.command.DebitMoneyCommand;
import com.example.demoaxon.event.AccountActivatedEvent;
import com.example.demoaxon.event.AccountCreatedEvent;
import com.example.demoaxon.event.AccountHeldEvent;
import com.example.demoaxon.event.MoneyCreditedEvent;
import com.example.demoaxon.event.MoneyDebitedEvent;
import lombok.Data;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@Data
public class AccountAggregate {

    @AggregateIdentifier
    private String id;
    private double accountBalance;
    private String currency;
    private String status;

    public AccountAggregate() {
    }

    @CommandHandler
    public AccountAggregate(CreateAccountCommand command){
        AggregateLifecycle.apply(new AccountCreatedEvent(command.id, command.accountBalance, command.currency));
    }

    @EventSourcingHandler
    protected void on(AccountCreatedEvent event){
        this.id= event.id;
        this.accountBalance= event.accountBalance;
        this.currency= event.currency;
        this.status= "CREATED";
    }

    @EventSourcingHandler
    protected void on(AccountActivatedEvent accountActivatedEvent){
        this.status = String.valueOf(accountActivatedEvent.status);
    }

    @CommandHandler
    protected void on(CreditMoneyCommand creditMoneyCommand){
        AggregateLifecycle.apply(new MoneyCreditedEvent(creditMoneyCommand.id, creditMoneyCommand.creditAmount, creditMoneyCommand.currency));
    }

    @EventSourcingHandler
    protected void on(MoneyCreditedEvent moneyCreditedEvent){

        if (this.accountBalance < 0 && (this.accountBalance + moneyCreditedEvent.amount) >= 0){
            AggregateLifecycle.apply(new AccountActivatedEvent(this.id, "ACTIVATED"));
        }

        this.accountBalance += moneyCreditedEvent.amount;
    }

    @CommandHandler
    protected void on(DebitMoneyCommand debitMoneyCommand){
        AggregateLifecycle.apply(new MoneyDebitedEvent(debitMoneyCommand.id, debitMoneyCommand.debitAmount, debitMoneyCommand.currency));
    }

    @EventSourcingHandler
    protected void on(MoneyDebitedEvent moneyDebitedEvent){

        if (this.accountBalance >= 0 && (this.accountBalance - moneyDebitedEvent.amount) < 0){
            AggregateLifecycle.apply(new AccountHeldEvent(this.id, "HOLD"));
        }

        this.accountBalance -= moneyDebitedEvent.amount;

    }

    @EventSourcingHandler
    protected void on(AccountHeldEvent accountHeldEvent){
        this.status = String.valueOf(accountHeldEvent.status);
    }
}
