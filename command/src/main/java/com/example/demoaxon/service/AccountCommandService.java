package com.example.demoaxon.service;


import com.example.demoaxon.dto.AccountCreateDTO;
import com.example.demoaxon.dto.MoneyCreditDTO;
import com.example.demoaxon.dto.MoneyDebitDTO;

import java.util.concurrent.CompletableFuture;

public interface AccountCommandService {
     CompletableFuture<String> createAccount(AccountCreateDTO accountCreateDTO);
     CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO moneyCreditDTO);
     CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO moneyDebitDTO);
}
