package com.example.demoaxon.service;


import com.example.demoaxon.command.CreateAccountCommand;
import com.example.demoaxon.command.CreditMoneyCommand;
import com.example.demoaxon.command.DebitMoneyCommand;
import com.example.demoaxon.dto.AccountCreateDTO;
import com.example.demoaxon.dto.MoneyCreditDTO;
import com.example.demoaxon.dto.MoneyDebitDTO;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class AccountCommandServiceImp implements AccountCommandService {

    private final CommandGateway commandGateway;

    public AccountCommandServiceImp(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public CompletableFuture<String> createAccount(AccountCreateDTO accountCreateDTO) {
        return commandGateway.send(new CreateAccountCommand(UUID.randomUUID().toString(), accountCreateDTO.getInitBalance(), accountCreateDTO.getCurrency()));
    }

    @Override
    public CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO moneyCreditDTO) {
        return commandGateway.send(new CreditMoneyCommand(accountNumber, moneyCreditDTO.getAmount(), moneyCreditDTO.getCurrency()));
    }

    @Override
    public CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO moneyDebitDTO) {
        return commandGateway.send(new DebitMoneyCommand(accountNumber, moneyDebitDTO.getAmount(), moneyDebitDTO.getCurrency()));
    }
}