package com.example.demoaxon.dto;

import lombok.Data;

@Data
public class AccountCreateDTO {
    double initBalance;
    String currency;
}
