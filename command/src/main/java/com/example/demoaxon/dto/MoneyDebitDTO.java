package com.example.demoaxon.dto;

import lombok.Data;

@Data
public class MoneyDebitDTO {
    double amount;
    String currency;
}
