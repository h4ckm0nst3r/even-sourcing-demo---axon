package com.example.demoaxon.dto;

import lombok.Data;

@Data
public class MoneyCreditDTO {
    double amount;
    String currency;
}
