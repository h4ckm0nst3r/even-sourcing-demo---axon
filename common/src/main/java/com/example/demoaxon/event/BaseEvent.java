package com.example.demoaxon.event;

public class BaseEvent<T> {

    public final T id;

    BaseEvent(T id) {
        this.id = id;
    }
}
