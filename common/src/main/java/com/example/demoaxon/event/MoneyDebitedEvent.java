package com.example.demoaxon.event;

public class MoneyDebitedEvent extends BaseEvent<String> {
    public final double amount;
    public final String currency;

    public MoneyDebitedEvent(String id, double amount, String currency) {
        super(id);
        this.amount = amount;
        this.currency = currency;
    }
}
