package com.example.demoaxon.event;

public class MoneyCreditedEvent extends BaseEvent<String> {

    public final double amount;
    public final String currency;

    public MoneyCreditedEvent(String id, double amount, String currency) {
        super(id);
        this.amount = amount;
        this.currency = currency;
    }
}
