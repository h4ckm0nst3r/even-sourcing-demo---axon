package com.example.demoaxoneventsource.querydemo.entity;

import com.example.demoaxon.event.AccountCreatedEvent;
import com.example.demoaxon.event.MoneyCreditedEvent;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountEventHandler {
    Logger logger= LoggerFactory.getLogger(AccountEventHandler.class);

    @Autowired
    AccountRepository repository;

    @EventHandler
    public void on(AccountCreatedEvent event){
        logger.info("On CreatedEvent");
        AccountEntity entity= new AccountEntity();
        entity.setCurrency(event.currency);
        entity.setAccountBalance(event.accountBalance);
        entity.setId(event.id);
        repository.save(entity);
    }

    @EventHandler
    public void on(MoneyCreditedEvent event){
        logger.info("On MoneyCreditedEvent");
        AccountEntity accountEntity= repository.findById(event.id).orElse(null);
        if(accountEntity!=null){
            accountEntity.setAccountBalance(accountEntity.getAccountBalance()+ event.amount);
            repository.save(accountEntity);
        }
    }
}
