package com.example.demoaxoneventsource.querydemo.aggregate;


import com.example.demoaxon.command.CreateAccountCommand;
import com.example.demoaxon.command.CreditMoneyCommand;
import com.example.demoaxon.command.DebitMoneyCommand;
import com.example.demoaxon.event.AccountActivatedEvent;
import com.example.demoaxon.event.AccountCreatedEvent;
import com.example.demoaxon.event.AccountHeldEvent;
import com.example.demoaxon.event.MoneyCreditedEvent;
import com.example.demoaxon.event.MoneyDebitedEvent;
import lombok.Data;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@Data
public class AccountAggregate {

    @AggregateIdentifier
    private String id;
    private double accountBalance;
    private String currency;
    private String status;

    public AccountAggregate() {
    }


}
