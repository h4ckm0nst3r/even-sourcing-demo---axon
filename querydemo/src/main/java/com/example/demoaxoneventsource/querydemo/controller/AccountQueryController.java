package com.example.demoaxoneventsource.querydemo.controller;

import com.example.demoaxoneventsource.querydemo.entity.AccountEntity;
import com.example.demoaxoneventsource.querydemo.service.AccountQueryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/bank-accounts")
@Api(value = "Account Queries", tags = "Account Queries")
@SwaggerDefinition(tags = {@Tag(name = "Account Queries", description = "Account Query Events Endpoint" )})
public class AccountQueryController {

    private final AccountQueryService accountQueryService;

    public AccountQueryController(AccountQueryService accountQueryService) {
        this.accountQueryService = accountQueryService;
    }

    @GetMapping("/{accountNumber}/events")
    public List<Object> listEventsForAccount(@PathVariable(value = "accountNumber") String accountNumber){
        return accountQueryService.listEventsForAccount(accountNumber);
    }

    @GetMapping("/{accountNumber}")
    public AccountEntity getAccount(@PathVariable(value = "accountNumber") String accountNumber){
        return accountQueryService.getAccount(accountNumber);
    }

    @GetMapping
    public List<AccountEntity> getAllAccounts(){
        return accountQueryService.getAllAccount();
    }
}
