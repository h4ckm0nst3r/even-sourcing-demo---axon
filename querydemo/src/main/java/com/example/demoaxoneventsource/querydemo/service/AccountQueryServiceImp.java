package com.example.demoaxoneventsource.querydemo.service;

import com.example.demoaxoneventsource.querydemo.entity.AccountEntity;
import com.example.demoaxoneventsource.querydemo.entity.AccountRepository;
import org.apache.commons.collections4.IterableUtils;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountQueryServiceImp implements AccountQueryService {

    private final EventStore eventStore;
    private final AccountRepository repository;

    public AccountQueryServiceImp(EventStore eventStore, AccountRepository repository) {
        this.eventStore = eventStore;
        this.repository= repository;
    }

    @Override
    public List<Object> listEventsForAccount(String accountNumber) {
        return eventStore.readEvents(accountNumber).asStream().map(Message::getPayload).collect(Collectors.toList());
    }

    @Override
    public AccountEntity getAccount(String accountNumber) {
        return repository.findById(accountNumber).orElse(null);
    }

    @Override
    public List<AccountEntity> getAllAccount() {
        return IterableUtils.toList(repository.findAll()) ;
    }
}
