package com.example.demoaxoneventsource.querydemo.entity;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountEntity, String> {
}
