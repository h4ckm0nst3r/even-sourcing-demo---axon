package com.example.demoaxoneventsource.querydemo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class AccountEntity {
    @Id
    private String id;

    private double accountBalance;

    private String currency;

    private String status;
}
