package com.example.demoaxoneventsource.querydemo.service;


import com.example.demoaxoneventsource.querydemo.entity.AccountEntity;

import java.util.List;

public interface AccountQueryService {
     List<Object> listEventsForAccount(String accountNumber);
     AccountEntity getAccount(String accountNumber);

    List<AccountEntity> getAllAccount();
}
